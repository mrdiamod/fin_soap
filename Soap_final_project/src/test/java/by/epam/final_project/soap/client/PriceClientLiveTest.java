package by.epam.final_project.soap.client;

import static org.junit.Assert.assertEquals;

import by.epam.final_project.soap.gen.GetPriceResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;


//Ensure that the server - by.epam.final_project.Application - is running before executing this test
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PriceClientConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class PriceClientLiveTest {

    @Autowired
    PriceClient client;

    @Test
    public void givenPriceService_whenPriceById_thenPriceIsValue300() {
        GetPriceResponse response = client.getPrice(1L);
        assertEquals(500, response.getPrice().getValue());
    }

}

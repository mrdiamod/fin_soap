package by.epam.final_project;

import by.epam.final_project.soap.Endpoint.PriceEndpoint;
import by.epam.final_project.soap.repository.PriceRepository;
import by.epam.final_project.soap.gen.*;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapFinalProjectApplication {
    private static final Logger LOGGER = Logger.getLogger(SoapFinalProjectApplication.class);

    public static void main(String[] args) {
		LOGGER.info("start...wait...");
        SpringApplication.run(SoapFinalProjectApplication.class, args);
		LOGGER.info("Soap service it's staring!");
		/** This is IMPORTLAND CODE*/
		GetPriceRequest getPriceRequest = new GetPriceRequest();
		getPriceRequest.setProductId(2);
		PriceRepository priceRepository = new PriceRepository();
		PriceEndpoint priceEndpoint = new PriceEndpoint(priceRepository);
		GetPriceResponse response = priceEndpoint.getPrice(getPriceRequest);
		LOGGER.info(response.getPrice());
		/***/

    }

	/*@Bean
    CommandLineRunner lookup(PriceClient quoteClient) {
		LOGGER.info("12");
		return args -> {
			Long country = Long.valueOf(1);

			if (args.length > 0) {
				country = Long.valueOf(args[0]);
			}
			GetPriceResponse response = quoteClient.getPrice(country);
			LOGGER.info(response.getPrice());
		};
	}*/
}

package by.epam.final_project.soap.client;

import by.epam.final_project.soap.gen.GetPriceRequest;
import by.epam.final_project.soap.gen.GetPriceResponse;
import org.apache.log4j.Logger;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class PriceClient extends WebServiceGatewaySupport {
    private static final Logger LOGGER = Logger.getLogger(PriceClient.class);


    public GetPriceResponse getPrice(Long productID) {
        LOGGER.info("PriceClient getPrice");
        GetPriceRequest request = new GetPriceRequest();
        request.setProductId(productID);
        LOGGER.info("Requesting prices for " + productID);
        GetPriceResponse response = (GetPriceResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        "http://localhost:8070/ws/prices",
                        request,
                        new SoapActionCallback("http://www.epam.by/final_project/soap/gen/GetPriceRequest"));
        return response;
    }

    public GetPriceResponse setPrice(Long productID) {
        LOGGER.info("PriceClient setPrice");
        GetPriceRequest request = new GetPriceRequest();
        request.setProductId(productID);
        LOGGER.info("Requesting prices for " + productID);
        GetPriceResponse response = (GetPriceResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        "http://localhost:8070/ws/prices",
                        request,
                        new SoapActionCallback("http://www.epam.by/final_project/soap/gen/GetPriceRequest"));
        return response;
    }

}

package by.epam.final_project.soap.repository;


import by.epam.final_project.soap.gen.CurrencyEnum;
import by.epam.final_project.soap.gen.Price;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PriceRepository {
    private static final Logger LOGGER = Logger.getLogger(PriceRepository.class);
    private static final Map<Long, Price> prices = new HashMap<>();

    @PostConstruct
    public void initData() {

        Price priceProd1 = new Price();
        priceProd1.setProductId(1);
        priceProd1.setValue(500);
        priceProd1.setCurrency(CurrencyEnum.EUR);
        prices.put(priceProd1.getProductId(), priceProd1);

        Price priceProd2 = new Price();
        priceProd2.setProductId(2);
        priceProd2.setValue(300);
        priceProd2.setCurrency(CurrencyEnum.USD);
        prices.put(priceProd2.getProductId(), priceProd2);

        Price priceProd3 = new Price();
        priceProd3.setProductId(3);
        priceProd3.setValue(200);
        priceProd3.setCurrency(CurrencyEnum.BYN);
        prices.put(priceProd3.getProductId(), priceProd3);
    }

    public List<Price> findPriceRange(Long minValue, Long maxValue) {
        LOGGER.info("findPriceRange");
        List<Price> priceList = new ArrayList<>();
        Assert.notNull(minValue, "The min value must not be null");
        Assert.notNull(maxValue, "The max value must not be null");
        for (Price price : prices.values()) {
            if (price.getValue() >= minValue && price.getValue() <= maxValue){
                priceList.add(price);
            }
        }
        return priceList;
    }

    public Price findPrice(Long productId) {
        LOGGER.info("findPrice");
        Assert.notNull(productId, "The id must not be null");
        return prices.get(productId);
    }

    public boolean createPrice(Price price) {
        LOGGER.info("createPrice");
        boolean statusCreate = false;
        Assert.notNull(price, "The price must not be null");
        if (prices.get(price.getProductId()) == null) {
            prices.put(price.getProductId(), price);
            statusCreate = true;
        }
        return statusCreate;
    }

    public boolean updatePrice(Price price) {
        LOGGER.info("updatePrice");
        boolean statusUpdate = false;
        Assert.notNull(price, "The price must not be null");
        if (prices.get(price.getProductId()) != null) {
            prices.put(price.getProductId(), price);
            statusUpdate = true;
        }
        return statusUpdate;
    }

    public boolean detelePrice(Long productId) {
        boolean statusDelete = false;
        LOGGER.info("detelePrice");
        Assert.notNull(productId, "The 'productId' must not be null");
        if (prices.get(productId) != null) {
            prices.remove(productId);
            statusDelete = true;
        }
        return statusDelete;
    }

    /**
     * getter's
     */
    public static Map<Long, Price> getStaticPrices() {
        return prices;
    }

    public Map<Long, Price> getPrices() {
        return prices;
    }
}

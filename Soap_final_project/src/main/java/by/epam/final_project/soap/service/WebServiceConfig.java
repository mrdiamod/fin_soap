package by.epam.final_project.soap.service;

import org.apache.log4j.Logger;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.ApplicationContext;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
    private static final Logger LOGGER = Logger.getLogger(WebServiceConfig.class);
    @Bean
    public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> containerCustomizer() {
        LOGGER.info("WebServiceConfig containerCustomizer");
        return (factory -> factory.setPort(8070));
    }

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        LOGGER.info("WebServiceConfig messageDispatcherServlet");
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/ws/*");
    }

    @Bean(name = "prices")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema pricesSchema) {
        LOGGER.info("WebServiceConfig defaultWsdl11Definition");
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("PricesPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://www.epam.by/final_project/soap/gen");
        wsdl11Definition.setSchema(pricesSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema pricesSchema() {
        LOGGER.info("WebServiceConfig pricesSchema");
        return new SimpleXsdSchema(new ClassPathResource("prices.xsd"));
    }
}

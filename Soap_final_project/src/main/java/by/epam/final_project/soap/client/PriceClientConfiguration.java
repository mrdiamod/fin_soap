package by.epam.final_project.soap.client;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class PriceClientConfiguration {
    private static final Logger LOGGER = Logger.getLogger(PriceClientConfiguration.class);

    @Bean
    public Jaxb2Marshaller marshaller() {
        LOGGER.info("PriceClientConfiguration marshaller");
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in
        // pom.xml by.epam.final_project.soap.wsdl
        marshaller.setContextPath("by.epam.final_project.soap.gen");
        return marshaller;
    }

    @Bean
    public PriceClient priceClient(Jaxb2Marshaller marshaller) {
        LOGGER.info("PriceClientConfiguration priceClient");
        PriceClient client = new PriceClient();
        client.setDefaultUri("http://localhost:8070/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}

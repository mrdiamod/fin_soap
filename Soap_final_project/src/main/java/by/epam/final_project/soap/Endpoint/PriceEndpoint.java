package by.epam.final_project.soap.Endpoint;

import by.epam.final_project.soap.gen.*;
import by.epam.final_project.soap.repository.PriceRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PriceEndpoint {
    private static final Logger LOGGER = Logger.getLogger(PriceEndpoint.class);
    private static final String NAMESPACE_URI = "http://www.epam.by/final_project/soap/gen";
    private PriceRepository priceRepository;

    @Autowired
    public PriceEndpoint(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPricesRangeRequest")
    @ResponsePayload
    public GetPricesRangeResponse getPricesRange(@RequestPayload GetPricesRangeRequest request) {
        LOGGER.info("getPricesRange");
        GetPricesRangeResponse response = new GetPricesRangeResponse();
        Long minValue = request.getMinValue();
        Long maxValue = request.getMaxValue();
        response.getPricelist().addAll(priceRepository.findPriceRange(minValue, maxValue));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPriceRequest")
    @ResponsePayload
    public GetPriceResponse getPrice(@RequestPayload GetPriceRequest request) {
        LOGGER.info("getPrice");
        GetPriceResponse response = new GetPriceResponse();
        response.setPrice(priceRepository.findPrice(request.getProductId()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createPriceRequest")
    @ResponsePayload
    public CreatePriceResponse createPrice(@RequestPayload CreatePriceRequest request) {
        LOGGER.info("createPrice");
        CreatePriceResponse response = new CreatePriceResponse();
        Price addPrice = new Price();
        addPrice.setProductId(request.getProductId());
        addPrice.setValue(request.getValue());
        addPrice.setCurrency(request.getCurrency());
        if (priceRepository.createPrice(addPrice)) {
            response.setStatus("OK");
            response.setPrice(priceRepository.findPrice(request.getProductId()));
        } else response.setStatus("ERROR CREATE");

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updatePriceRequest")
    @ResponsePayload
    public UpdatePriceResponse updatePrice(@RequestPayload UpdatePriceRequest request) {
        LOGGER.info("updatePrice");
        UpdatePriceResponse response = new UpdatePriceResponse();
        Price updatePrice = new Price();
        updatePrice.setProductId(request.getProductId());
        updatePrice.setValue(request.getValue());
        updatePrice.setCurrency(request.getCurrency());
        if (priceRepository.updatePrice(updatePrice)) {
            response.setStatus("OK");
            response.setPrice(priceRepository.findPrice(request.getProductId()));
        } else response.setStatus("ERROR UPDATE");
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deletePriceRequest")
    @ResponsePayload
    public DeletePriceResponse deletePrice(@RequestPayload DeletePriceRequest request) {
        LOGGER.info("deletePrice");
        DeletePriceResponse response = new DeletePriceResponse();
        if (priceRepository.detelePrice(request.getProductId())) {
            response.setStatus("OK");
        } else response.setStatus("ERROR DELETE");
        return response;
    }

}

